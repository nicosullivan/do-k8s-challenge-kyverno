# Setup
## Required Tools
- kubectl
- pulumi https://www.pulumi.com/docs/get-started/install/

If you don't want to signup for pulumi you can use a local state file by logging in 'locally' `mkdir .state && pulumi login file://.state` this will keep this setup self contained. Once you run `pulumi up` locally you'll be prompted to create a passphrase for protecting secrets. This doesn't need to be crazy complex as this is just an example, you will need this in the teardown step. 

After installing pulumi you need to ensure you have a r/w digitalocean api token in the env var $DIGITALOCEAN_TOKEN

# Reproduce 
- `npm install`
- `pulumi up` this will create the cluster and deploy kyverno and any policies in the `./policies` folder
- `kubectl apply -f disallowed-resources/deployment.yaml` this will fail as it doesn't match the policy.
- `kubectl apply -f allowed-resources/deployment.yaml` this will be allowed as it satisfies the policy

# Teardown
If you used the local file for state you will need to prefix the following with `PULUMI_CONFIG_PASSPHRASE=[your-passphrase]`

`pulumi destroy && cd .. && rm -rf do-k8s-challenge-kyverno`

To uninstall pulumi simply `rm -rf ~/.pulumi` and remove that path from $PATH. Pulumi usually adds this in your `~/.bashrc`