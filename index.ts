import * as pulumi from "@pulumi/pulumi"
import * as digitalocean from "@pulumi/digitalocean";
import * as k8s from "@pulumi/kubernetes";

const project = new digitalocean.Project(pulumi.getProject())

const cluster = new digitalocean.KubernetesCluster(`${pulumi.getProject()}-${pulumi.getStack()}-cluster`, {
    region: digitalocean.Region.NYC3,
    version: "1.21.5-do.0",
    autoUpgrade: false,
    surgeUpgrade: false,
    nodePool: {
        name: "default",
        size: digitalocean.DropletSlug.DropletS1VCPU2GB,
        nodeCount: 2,
    },
});

const resource = new digitalocean.ProjectResources("cluster", {
    project: project.id,
    resources: [cluster.clusterUrn]
})

const kubeconfig = cluster.status.apply(status => {
    if (status === "running") {
        const clusterDataSource = cluster.name.apply(name => digitalocean.getKubernetesCluster({name}));
        return clusterDataSource.kubeConfigs[0].rawConfig;
    } else {
        return cluster.kubeConfigs[0].rawConfig;
    }
})

const provider = new k8s.Provider("do-k8s", { kubeconfig });

const namespace = new k8s.core.v1.Namespace('kyverno', {
    metadata: {
        name: 'kyverno',
    },
}, { provider });

const release = new k8s.helm.v3.Release('kyverno', {
    chart: 'kyverno',
    namespace: namespace.metadata.name,
    repositoryOpts: {
        repo: 'https://kyverno.github.io/kyverno/',
    },
    values: {},
}, { provider, dependsOn: [namespace] })

const policies = new k8s.yaml.ConfigGroup("kyverno-policies", {
    files: "policies/*.yaml",
}, { provider, dependsOn: [ release ] });
